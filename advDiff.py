from numTools import *
import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.interpolate import interp1d
plt.ion()

class advDiff(object):

    def __init__(self, Ns, N=10., ds=0.004, dl=0.006, hw=3.1,
                 new_formulation=False):
        #Geometrical parameters
        self.Ns = Ns    #Number of layers of small particles (in ds)
        self.N = N      #Mixture number of layers (in dl)
        self.dl = dl    #Diameter of large particles
        self.ds = ds    #Diameter of small particles
        self.Nl = self.N-self.Ns*self.ds/self.dl    #Number of layers of large particles
        self.H = ((0.5+self.Nl)*self.dl + self.Ns*self.ds)/self.dl #Dimensionless height of the bed mixture
        self.hw = hw #Water depth (in large particle diameter)
        self.new_formulation = new_formulation #To chosse between the new or old
                        #formulation of advection and diffusion coefficients

        #Physical parameters
        self.g = 9.81       #Gravity constant
        self.rhop = 2500.    #Particle density
        self.rhof = 1000.    #Fluid denisty
        self.phiMax = 0.61  #Random close packing
        self.slope = 0.1    #Bed slope

        self.make_grid()
        self.rheology()

    def make_grid(self, dz=None):
        #Build the discretized spatial grid
        self.zEnd = self.H
        if dz is None: self.dz=self.zEnd/80
        else: self.dz = dz
        self.z = np.linspace(0,self.zEnd, int(self.zEnd/self.dz)+1)
        self.dz = self.z[1]-self.z[0]

    def rheology(self):
        '''
        Compute the shear rate, shear stress and granular pressure
        from DEM simulation.
        '''
        #Shear rate given by a fit on the DEM simulation
        gamma0 = 1.68e-7
        s0 = 0.75
        self.gamma = gamma0*np.exp(self.z/s0) #Dimensionless shear rate
        #Dimensionless shear stress profile obtained from multiphase flow equations
        self.tau = self.rhof/self.rhop*np.sin(self.slope)*self.hw +\
                (self.phiMax+(1-self.phiMax)*self.rhof/self.rhop)*np.sin(self.slope)*(self.H-self.z)
        self.etap = self.tau/self.gamma #mixture granular viscosity
        #Dimensionless pressure gradient and pressure
        self.dpmdz = -self.phiMax*(self.rhop-self.rhof)/self.rhop*np.cos(self.slope)
        self.pm = -self.dpmdz*(self.H-self.z)
        #Friction coefficient
        self.mu = self.tau/(1e-6+self.pm)
        #Inertial number
        self.I = self.gamma/np.sqrt(1e-6+self.pm)
        #Sr0 Computed from the DEM simulations
        self.Sr0 = 0.049

    def adv_diff_coeff(self, phi):
        '''
        Compute the advection and diffusion coefficients
        '''
        #Drag coefficient
        if self.new_formulation is not True:
            self.Cd = 3
        else:
            self.Cd = 3+28*phi
        #Particle Stokes number (computed from dimensionless viscosity)
        self.St = 1./ (6*self.Cd)*self.I/(self.mu*np.sqrt(1e-6 + self.pm))
        #Segregation Force
        if self.new_formulation is not True:
            self.Fmu = 1.
        else:
            #Size ratio dependency
            self.f_r = 0.45*(np.exp((self.dl/self.ds-1)/0.63)-1)/0.54515487311213884
            self.Fmu = self.f_r*6*self.Sr0*self.Cd*self.rhop/(self.phiMax*(self.rhop-self.rhof)) *\
                    self.mu*np.sqrt(self.pm)*self.I**(-0.15)
        #Advection coefficient
        self.Sr = self.Fmu*self.St*self.dpmdz
        self.SrF = interp1d(self.z, self.Sr, fill_value='extrapolate') #Create
        #a function allowing to compute the value of Sr in the cell edges (z_j+1/2) 
        #Diffusion coefficient
        self.D = phi*self.pm*self.St/self.phiMax
        self.DF = interp1d(self.z, self.D, fill_value='extrapolate')

    def compute_CFL(self, phi, verbose=False):
        """
        For a Godunov scheme, CFL condition is: max(S_r(z))dt/dz < 1/2
        dt = dz/(2*max(|Sr|))
        For diffusion, the CFL condition is: max(|D|)dt/dz^2 < 1/2
        dt = min(dz/(2*max(|Sr|)), dz^2/(2*max(|D|)))
        """
        self.dt_CFL = self.dz/(2*np.max(np.abs(self.Sr*(2*phi-1))))
        self.dt_diff = self.dz**2/(2*np.max(np.abs(self.D)))
        dt = min(self.dt_CFL, self.dt_diff)

        self.dt = dt
        if verbose:
            print("dt = {}".format(dt))
            print("CFL condition : {}".format(np.max(np.abs(derivate(self.z,self.Sr))) * self.dt/self.dz))
            print("Stability condition for diffusion : {}".format(np.max(np.abs(self.D))*self.dt/self.dz**2))

    def initialization(self, phi0=None, fromFile=False):
        """
        Initialization of the problem:
            Compute the initial solution (step solution by default)
            Create time array and phi array
            Initiate advection and diffusiopn coefficients
            Compute the CFL condition
        """
        #Compute initial solution
        if fromFile is True:
            phi0 = self.load_initial_solution()
        elif phi0 is None:
            phi0 = self.step_initial_solution()
        else:
            if len(phi0)!=len(self.z):
                raise ValueError("The shape of phi0 has to be the same than the shape of z : len(phi0) should be {}".format(len(self.z)))

        #Initiate arrays
        self.phi = [phi0] #array with all saved solutions
        self.t = 0.0 #Current time
        self.time = [self.t] #Array with all saved times

        #Initiate the advection and diffusion coefficients
        self.adv_diff_coeff(phi0)

        #Compute the CFL condition
        self.compute_CFL(phi0)

    def step_initial_solution(self):
        phi0 = np.zeros(len(self.z))
        for i in range(len(self.z)):
            if self.z[i] < (0.5+self.Nl): phi0[i] = 0
            else: phi0[i] = 1
        return phi0

    def Fs(self, z, phi):
        """
        Advection flux function
        """
        return self.SrF(z)*phi*(1-phi)

    def godunov_fluxes(self, phi):
        '''
        Compute Godunov fluxes in an array like form (faster for computations)
        as a function of \phi_s.
        Return the total godunov scheme in an array like form
        -self.dt/self.dz*(F_up-F_down)
        '''
        z_up = self.z+self.dz/2 #corresponds to z_{j+1/2}
        z_down = self.z-self.dz/2 #corresponds to z_{j-1/2}
        #Compute phi_s_{j+1/2} and pi_s_{j-1/2}
        phi_up = np.concatenate((phi[1:],[phi[-2]]))
        phi_down = np.concatenate(([phi[1]],phi[:-1]))
        #Compute F_{j+1/2} and F_{j-1/2}
        F_up = np.maximum(self.Fs(z_up,np.maximum(phi,0.5)),self.Fs(z_up,np.minimum(phi_up,0.5)))
        F_down = np.maximum(self.Fs(z_down,np.maximum(phi_down,0.5)),self.Fs(z_down,np.minimum(phi,0.5)))
        F_up[-1] = -F_up[-2] #Top boundary condition F_J = -F_{J-1/2}
        F_down[0] = -F_down[1] #Bottom boundary condition F_0 = -F_{1/2}

        return -self.dt/self.dz*(F_up-F_down)

    def diffusion_matrix(self, phi):
        '''
        Construct the diffusion matrix which has to be computed at each time
        step because the diffusion coefficient depends on \phi_s
        '''
        z_up = self.z[:-1]+self.dz/2 #Corresponds to z_{j+1/2} without z_J
        z_down = self.z[1:]-self.dz/2 #Corresponds to z_{j-1/2} without z_0
        D_sup = self.DF(z_up) #Diag sup 
        D_down = self.DF(z_down) #Diag down
        z_up = np.concatenate((z_up, [self.z[-1]])) #add z_J
        z_down = np.concatenate(([self.z[0]],z_down)) #add z_0
        diag = -(self.DF(z_down)+self.DF(z_up)) #Diag (contains one more element than D_sup and D_down)
        A = np.diag(D_down,-1)+np.diag(diag,0)+np.diag(D_sup,1) #Build the matrix
        A = np.matrix(A)
        #Boundary conditions
        A[0,0] = -2*D_sup[0]
        A[0,1] = 2*D_sup[0]
        A[-1,-2] = 2*D_down[-1]
        A[-1,-1] = -2*D_down[-1]
        self.diffMat = A

    def diffusion_fluxes(self, phi):
        self.diffusion_matrix(phi)
        phi_temp = phi.reshape(len(self.z),1)
        phi_temp = self.diffMat*phi_temp
        phi_temp = np.array(phi_temp.reshape((len(self.z),)))[0]
        return self.dt/self.dz**2*phi_temp

    def time_step(self, phi):
        """
        Perform a time step, with a Godunov scheme.
        - phi, solution at time t, np.array of the same size than self.z
        - return phi at time t+dt
        """
        #Update the advection and diffusion coefficient with the new value of phi
        self.adv_diff_coeff(phi)
        self.compute_CFL(phi)
        #Initiate the fluxes
        fluxes = np.zeros(len(phi))
        #Advection
        godFlux = self.godunov_fluxes(phi)
        fluxes+=godFlux
        #Diffusion
        diffFlux = self.diffusion_fluxes(phi)
        fluxes+=diffFlux
        #Add fluxes
        phi += fluxes
        self.t += self.dt
        return phi

    def num_resolution(self, tEnd, dt_save=1):
        if dt_save is None: self.dt_save=self.dt
        else: self.dt_save = dt_save

        print('Simulation started !!')
        phi_new=1.*self.phi[-1]
        while self.t < tEnd:
            phi_new = self.time_step(1.*phi_new)
            if self.t >= self.time[-1]+self.dt_save:
                self.phi.append(phi_new)
                self.time.append(self.t)
            print('t = {}\r'.format(self.t)),
            sys.stdout.flush()

    def plot(self, levels=None, cmap='RdYlGn'):
        if levels is None: levels = np.linspace(-1e-6,1,26)
        plt.figure()
        plt.contourf(self.time, self.z, np.transpose(self.phi), levels,
                     cmap=cmap)
        plt.colorbar()
        plt.xlabel(r'$t$', fontsize=25)
        plt.ylabel(r'$z$', fontsize=25, rotation=True)
