from advDiff import *
import matplotlib.pyplot as plt
plt.ion()


a = advDiff(Ns=2, N=10., ds=0.003, dl=0.006, hw=3.1,  new_formulation=True)
a.initialization()
a.num_resolution(60000, dt_save=10)

a.massCenter = np.zeros(len(a.time))
for i in range(len(a.time)):
    a.massCenter[i] = integrate(a.z, a.phi[i]*a.z)/integrate(a.z, a.phi[i])

a.plot()
plt.figure()
plt.plot(a.time, a.massCenter)
plt.xlabel(r'$t$', fontsize=20)
plt.ylabel(r'$z_c$', rotation=True, horizontalalignment='right', fontsize=20)
